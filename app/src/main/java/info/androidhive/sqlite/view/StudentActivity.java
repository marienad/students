package info.androidhive.sqlite.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.students.R;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.sqlite.database.DatabaseHelper;
import info.androidhive.sqlite.database.model.Note;

public class StudentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);
        DatabaseHelper db = new DatabaseHelper(this);
        Note note = MainActivity.notesList.get(getIntent().getIntExtra("position",0));
        TextView firstName = findViewById(R.id.text_first_name);
        TextView lastName = findViewById(R.id.text_last_name);
        TextView patronymic = findViewById(R.id.text_patronymic);
        TextView group = findViewById(R.id.text_group);
        TextView age = findViewById(R.id.text_age);
        TextView faculty = findViewById(R.id.text_faculty);
        firstName.setText(note.getFirstName());
        lastName.setText(note.getLastName());
        patronymic.setText(note.getPatronymic());
        group.setText(note.getGroup());
        age.setText(Integer.toString(note.getAge()));
        faculty.setText(note.getFaculty());
    }
}
