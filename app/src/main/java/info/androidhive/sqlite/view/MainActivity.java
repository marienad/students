package info.androidhive.sqlite.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.students.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import info.androidhive.sqlite.database.DatabaseHelper;
import info.androidhive.sqlite.database.model.Note;
import info.androidhive.sqlite.utils.ImageHelper;
import info.androidhive.sqlite.utils.MyDividerItemDecoration;
import info.androidhive.sqlite.utils.RecyclerTouchListener;

public class MainActivity extends AppCompatActivity {
    private NotesAdapter mAdapter;
    public static List<Note> notesList = new ArrayList<>();
    private List<Note> listFiltered;
    private RecyclerView recyclerView;
    private final int PICK_IMAGE = 2;
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler_view);

        db = new DatabaseHelper(this);

        notesList.addAll(db.getAllNotes());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNoteDialog(false, null, -1);
            }
        });

        mAdapter = new NotesAdapter(this, notesList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);

        /**
         * On long press on RecyclerView item, open alert dialog
         * with options to choose
         * Edit and Delete
         * */
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Intent intent = new Intent(getApplicationContext(), StudentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("position", position);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
                showActionsDialog(position);
            }
        }));
    }

    /**
     * Deleting note from SQLite and removing the
     * item from the list by its position
     */
    private void deleteNote(int position) {
        // deleting the note from db
        db.deleteNote(notesList.get(position));

        // removing the note from the list
        notesList.remove(position);
        mAdapter.notifyItemRemoved(position);
    }

    /**
     * Opens dialog with Edit - Delete options
     * Edit - 0
     * Delete - 0
     */
    private void showActionsDialog(final int position) {
        CharSequence colors[] = new CharSequence[]{"Изменить", "Удалить"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Выбрать");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    showNoteDialog(true, notesList.get(position), position);
                } else {
                    deleteNote(position);
                }
            }
        });
        builder.show();
    }

    private void showNoteDialog(final boolean shouldUpdate, final Note note, final int position) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
        View view = layoutInflaterAndroid.inflate(R.layout.activity_form, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilderUserInput.setView(view);

        final EditText inputFirstName = view.findViewById(R.id.firstName);
        final EditText inputLastName = view.findViewById(R.id.lastName);
        final EditText inputPatronymic = view.findViewById(R.id.patronymic);
        final EditText inputGroup = view.findViewById(R.id.group);
        final EditText inputAge = view.findViewById(R.id.age);
        final EditText inputFaculty = view.findViewById(R.id.faculty);
        final EditText inputMore = view.findViewById(R.id.more);
        final TextView photoPath = findViewById(R.id.photo_path);
        TextView dialogTitle = view.findViewById(R.id.dialog_title);
        dialogTitle.setText(!shouldUpdate ? getString(R.string.lbl_new_note_title) : getString(R.string.lbl_edit_note_title));

        if (shouldUpdate && note != null) {
            inputFirstName.setText(note.getFirstName());
            inputLastName.setText(note.getLastName());
            inputPatronymic.setText(note.getPatronymic());
            inputGroup.setText(note.getGroup());
            inputAge.setText(Integer.toString(note.getAge()));
            inputFaculty.setText(note.getFaculty());
            inputMore.setText(note.getMore());
            photoPath.setText(note.getPhoto());
        }
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(shouldUpdate ? "обновить" : "сохранить", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                    }
                })
                .setNegativeButton("назад",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });
        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show toast message when no text is entered
                if (TextUtils.isEmpty(inputFaculty.getText().toString()) || TextUtils.isEmpty(inputFirstName.getText().toString()) ||
                        TextUtils.isEmpty(inputLastName.getText().toString()) ||
                        TextUtils.isEmpty(inputGroup.getText().toString()) ||
                        TextUtils.isEmpty(inputAge.getText().toString()) ||
                        TextUtils.isEmpty(inputMore.getText().toString())) {
                    Toast.makeText(MainActivity.this, "Заполните все поля!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    alertDialog.dismiss();
                }

                // check if user updating note
                if (shouldUpdate && note != null) {
                    // update note by it's id
                    updateNote(inputFirstName.getText().toString(), inputLastName.getText().toString(), inputPatronymic.getText().toString(),
                            inputGroup.getText().toString(), inputFaculty.getText().toString(), Integer.parseInt(inputAge.getText().toString()), String.valueOf(photoPath.getText()), inputMore.getText().toString(), position);
                } else {
                    // create new note
                    createNote(inputFirstName.getText().toString(), inputLastName.getText().toString(), inputPatronymic.getText().toString(),
                            inputGroup.getText().toString(), Integer.parseInt(inputAge.getText().toString()), inputFaculty.getText().toString(), String.valueOf(photoPath.getText()), inputMore.getText().toString());
                }
            }
        });
    }

    public void createNote(String firstName, String lastName, String patronymic, String group, int age, String faculty, String photo, String more) {
        // inserting note in db and getting
        // newly inserted note id
        long id = db.insertNote(firstName, lastName, patronymic, group, faculty, age, photo, more);

        // get the newly inserted note from db
        Note n = new Note(db.getNotesCount(), firstName, lastName, patronymic, group, faculty, age, photo, more);

        if (n != null) {
            notesList.add(0, n);
            mAdapter.notifyDataSetChanged();
        }
    }

    public void updateNote(String firstName, String lastName, String patronymic, String group, String faculty, int age, String photo, String more, int position) {
        Note n = new Note(position, firstName, lastName, patronymic, group, faculty, age, photo, more);

        db.updateNote(firstName, lastName, patronymic, group, faculty, age, photo, more, position);

        notesList.set(position, n);
        mAdapter.notifyItemChanged(position);
    }

    public void search(View view) {
        EditText editText = findViewById(R.id.search);
        String charString = editText.getText().toString();
        if (charString.isEmpty()) {
            notesList = listFiltered;
        } else {
            List<Note> filteredList = new ArrayList<>();
            for (Note note : notesList) {
                if (note.getFirstName().toLowerCase().contains(charString.toLowerCase()) || note.getLastName().toLowerCase().contains(charString.toLowerCase())) {
                    filteredList.add(note);
                }
            }
            listFiltered = filteredList;
        }
        recyclerView.setAdapter(new NotesAdapter(this, listFiltered));
    }

    public void getPhoto(View view) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            final Uri imageUri = data.getData();
            TextView textView = findViewById(R.id.photo_path);
            textView.setText((CharSequence) imageUri);
        }
    }
}